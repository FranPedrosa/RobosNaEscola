# coding : utf-7

import requests
import subprocess
import os

verde = '\033[92m'
amarelo = '\033[93m'
default = '\033[39m'

header = """Comandos:
	enviar [numero do programa] [numero do robo] - Programa o robo.
	abrir [numero do programa] - Abre o programa.
	ip - Lista as ESPs na rede, e salva nos robos.
	ping - Usar se ip não funcionar.
	grupo - Mostra todos os programas de um grupo.
	robos - Lista os robos
	novo [numero do robo] [endereço de ip] - Registra um robo em um novo IP.
	comeco [numero do programa] - Muda o primeiro programa que aparece.
	. - recarrega o log
	fim - termina o programa"""

def imprimir_log (first, enviados):
	os.system('clear')
	logs = requests.get('http://principia.icmc.usp.br:8080/uploads/log').text.split('\n')
	for log in logs[first:]:
		if log not in enviados:
			print(f'{amarelo}  {log}  {default}')
		else:
			print(f'{verde} {log}  {default}')
	print (header)

def imprimir_grupo (first, enviados, grupo):
	os.system('clear')
	logs = requests.get('http://principia.icmc.usp.br:8080/uploads/log').text.split('\n')
	for log in logs[first:]:
		if grupo in log:
			if log not in enviados:
				print(f'{amarelo}  {log}  {default}')
			else:
				print(f'{verde} {log}  {default}')
	print (header)

def imprimir_robos (robos):
	os.system('clear')
	for i,robo in enumerate(robos):
		print(str(i)+':'+robo)
	print (header)

if __name__ == '__main__':
	os.system('precompile.sh')
	logs = requests.get('http://principia.icmc.usp.br:8080/uploads/log').text.split('\n')
	first = len(logs) -1
	enviados = []
	loop = True
	robos = ['']*10
	imprimir_log(first, enviados)

	while loop:
		comando = input().split(' ')

		if comando[0] == 'comeco':
			first = int(comando[1]) + 1
			imprimir_log(first, enviados)
		elif comando[0] == '.':
			imprimir_log(first, enviados)
		elif comando[0] == 'fim':
			loop = False
		elif comando[0] == 'grupo':
			imprimir_grupo(first,enviados,comando[1])
		elif comando[0] == 'novo':
			robos[int(comando[1])] = comando[2]
			imprimir_robos(robos)
		elif comando[0] == 'robos':
			imprimir_robos(robos)
		elif comando[0] == 'ping':
			for i in range(1,255):
				os.system('ping -W 1 -c 1 192.168.43.'+str(i))
		elif comando[0] == 'ip':
			os.system('clear')
			os.system('arp -e -n | grep 3c > saida')
			saida = open('saida','r').readlines()
			for i,linha in enumerate(saida):
				robos[i] = linha.split(' ')[0]
			imprimir_robos(robos)
		elif comando[0] == 'abrir':
			os.system('clear')
			sketch = requests.get('http://principia.icmc.usp.br:8080/uploads/'+str(comando[1])+'.ino').text
			print(sketch)
			print(header)
		elif comando[0] == 'enviar':
			sketch = requests.get('http://principia.icmc.usp.br:8080/uploads/'+str(comando[1])+'.ino').text
			arquivo = open('download.ino','w')
			arquivo.write(sketch)
			arquivo.close()
			enviados.append(logs[int(comando[1]) + 1])
			os.system('sh uploadIP.sh download.ino ' + robos[int(comando[2])])
			imprimir_log(first, enviados)
