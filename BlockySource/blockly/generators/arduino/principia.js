var wifiDefinitions = "#include <WiFi.h>\n"+
"#include <ESPmDNS.h>\n"+
"#include <WiFiUdp.h>\n"+
"#include <ArduinoOTA.h>\n"+
"\n"+
"const char* ssid = \"RobosNaEscola\";\n"+
"const char* password = \"12345678\";\n"+
"\n"+
"void doWifi(){\n"+
	"ArduinoOTA.handle();\n"+
"}\n"+
"\n";

var wifiSetup = "Serial.begin(115200);\n"+
"  Serial.println(\"Booting\");\n"+
"  WiFi.mode(WIFI_STA);\n"+
"  while (WiFi.waitForConnectResult() != WL_CONNECTED) {\n"+
"    Serial.println(\"Connection Failed! Rebooting...\");\n"+
"    delay(5000);\n"+
"    ESP.restart();\n"+
"  }\n"+
"\n"+
"  ArduinoOTA\n"+
"    .onStart([]() {\n"+
"      String type;\n"+
"      if (ArduinoOTA.getCommand() == U_FLASH)\n"+
"        type = \"sketch\";\n"+
"      else // U_SPIFFS\n"+
"        type = \"filesystem\";\n"+
"\n"+
"      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()\n"+
"      Serial.println(\"Start updating \" + type);\n"+
"    })\n"+
"    .onEnd([]() {\n"+
"      Serial.println(\"\nEnd\");\n"+
"    })\n"+
"    .onProgress([](unsigned int progress, unsigned int total) {\n"+
"      Serial.printf(\"Progress: %u%%\r\", (progress / (total / 100)));\n"+
"    })\n"+
"    .onError([](ota_error_t error) {\n"+
"      Serial.printf(\"Error[%u]: \", error);\n"+
"      if (error == OTA_AUTH_ERROR) Serial.println(\"Auth Failed\");\n"+
"      else if (error == OTA_BEGIN_ERROR) Serial.println(\"Begin Failed\");\n"+
"      else if (error == OTA_CONNECT_ERROR) Serial.println(\"Connect Failed\");\n"+
"      else if (error == OTA_RECEIVE_ERROR) Serial.println(\"Receive Failed\");\n"+
"      else if (error == OTA_END_ERROR) Serial.println(\"End Failed\");\n"+
"    });\n"+
"\n"+
"  ArduinoOTA.begin();\n"+
"\n"+
"  Serial.println(\"Ready\");\n"+
"  Serial.print(\"IP address: \");\n"+
"  Serial.println(WiFi.localIP());\n";

var wifiCode = "  doWifi();\n";

var apDefinitions = '#include <ArduinoOTA.h>\n'+
'#include <WiFi.h>\n'+
'#include <WebServer.h>\n'+
'#define LED_BUILTIN 2\n'+
'const char *ssid = "ESP32 Access Point";\n'+
'const char *password = "your-password";\n'+
'WebServer server(80);\n'+
'void handleRoot() {\n'+
'  server.send(200, "text/plain", "Hello from ESP32 !");\n'+
'}\n'+
'void handleNotFound() {\n'+
'  String message = "File Not Found\\n";\n'+
'  message += "URI: ";\n'+
'  message += server.uri();\n'+
'  message += "\\nMethod: ";\n'+
'  message += (server.method() == HTTP_GET) ? "GET" : "POST";\n'+
'  message += "\\n"Arguments: ";\n'+
'  message += server.args();\n'+
'  message += "\\n";\n'+
'  for (uint8_t i = 0; i < server.args(); i++) {\n'+
'    message += " " + server.argName(i) + ": " + server.arg(i) + "\\n";\n'+
'  }\n'+
'  server.send(404, "text/plain", message);\n'+
'}\n'+
'void doWifi(){\n'+
'  ArduinoOTA.handle();\n'+
'  server.handleClient();\n'+
'}\n';

var apSetup ='  Serial.println("Booting");\n'+
'  WiFi.softAP(ssid, password);\n'+
'  Serial.print("Access Point ");\n'+
'  Serial.print(ssid);\n'+
'  Serial.println(" started");\n'+
'  Serial.print("IP address:   ");\n'+
'  Serial.println(WiFi.softAPIP());\n'+
'  server.on("/", handleRoot);\n'+
'  server.onNotFound(handleNotFound);\n'+
'  server.on("/ligaled", []() {\n    server.send(200, "text/plain", "ligou");\n'+
'    digitalWrite(LED_BUILTIN, HIGH);\n'+
'  });\n'+
'  server.on("/desligaled", []() {\n    server.send(200, "text/plain", "apagou");\n'+
'    digitalWrite(LED_BUILTIN, LOW);\n'+
'  });\n'+
'  server.begin();\n'+
'  Serial.println("HTTP server started");\n'+
'  ArduinoOTA.onStart([]() {\n    String type;\n'+
'    if (ArduinoOTA.getCommand() == U_FLASH) {\n      type = "sketch";\n'+
'    } else {\n      type = "filesystem";\n'+
'    }\n    Serial.println("Start updating " + type);\n'+
'  });\n'+
'  ArduinoOTA.onEnd([]() {\n    Serial.println("\\nEnd");\n'+
'  });\n'+
'  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {\n    Serial.printf("Progress: %u%%\\r", (progress / (total / 100)));\n'+
'  });\n'+
'  ArduinoOTA.onError([](ota_error_t error) {\n    Serial.printf("Error[%u]: ", error);\n'+
'    if (error == OTA_AUTH_ERROR) {\n      Serial.println("Auth Failed");\n'+
'    } else if (error == OTA_BEGIN_ERROR) {\n      Serial.println("Begin Failed");\n'+
'    } else if (error == OTA_CONNECT_ERROR) {\n      Serial.println("Connect Failed");\n'+
'    } else if (error == OTA_RECEIVE_ERROR) {\n      Serial.println("Receive Failed");\n'+
'    } else if (error == OTA_END_ERROR) {\n      Serial.println("End Failed");\n'+
'    }\n  });\n'+
'  ArduinoOTA.begin();\n'+
'  Serial.println("Ready");\n';

function codeDelay(tempo){
	return 'for(int i = 0; i < (int)' + tempo + '; i++) {\n'+
	'  delay(10);\n'+
	'\t' + wifiCode +
	'}\n';
}

var TTdefinition =  '#include <analogWrite.h>\n'+
'#define motor1PWM 23\n'+
'#define motor2PWM 22\n'+
'#define motor1Dir 21\n'+
'#define motor2Dir 19\n'+
'int velocidade = 100;\n';

var TTsetup = '  pinMode(motor1PWM, OUTPUT);\n'+
'	pinMode(motor2PWM, OUTPUT);\n'+
'	pinMode(motor1Dir, OUTPUT);\n'+
'	pinMode(motor2Dir, OUTPUT);\n'+
'	analogWriteFrequency(200);\n'+
'	analogWrite(motor1PWM, 0);\n'+
'	analogWrite(motor2PWM, 0);\n'+
'	digitalWrite(motor1Dir,0);\n'+
'	digitalWrite(motor2Dir,0);\n';

function TTcode(v1,v2,t){
	return'analogWrite(motor1PWM,'+v1+');\n'+
	' analogWrite(motor2PWM,'+v2+');\n'+
	' digitalWrite(motor1Dir,1);\n'+
	' digitalWrite(motor2Dir,1);\n'+
	' '+codeDelay(t);
}

Blockly.Arduino.serial_print = function() {
	var content = Blockly.Arduino.valueToCode(this, 'CONTENT', Blockly.Arduino.ORDER_ATOMIC) || '0';
	//content = content.replace('(','').replace(')','');

	Blockly.Arduino.setups_['setup_serial_' + profile.default.serial] = 'Serial.begin(' + profile.default.serial + ');\n';

	var code = 'Serial.print(' + content + ');\n';
	return code;
};

Blockly.Arduino.ESP32_WIFI = function() {
	Blockly.Arduino.definitions_['define_ESP32WIFI'] = wifiDefinitions;
	Blockly.Arduino.setups_['setup_serial_' + profile.default.serial] = 'Serial.begin(' + profile.default.serial + ');\n';
	Blockly.Arduino.setups_['setup_OTA_principia'] = wifiSetup; 
	return wifiCode;
};

Blockly.Arduino.ESP32_OTA_Server = function() {
	Blockly.Arduino.definitions_['define_pins_WIFIServer_OTA'] = apDefinitions;
	Blockly.Arduino.setups_['setup_serial_' + profile.default.serial] = 'Serial.begin(' + profile.default.serial + ');\n';
	Blockly.Arduino.setups_['setup_output_2'] = 'pinMode(LED_BUILTIN, OUTPUT);\n';
	Blockly.Arduino.setups_['setup_OTA_principia'] = apSetup;
	return wifiCode;
};

Blockly.Arduino.Principia_delay_ESP = function() {
	var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000';
	delay_time /= 10;
	return codeDelay(delay_time);
};

Blockly.Arduino.TiraTampa_Frente = function() {
	var TEMPO = Blockly.Arduino.valueToCode(this, 'TEMPO', Blockly.Arduino.ORDER_ATOMIC) || '0';
	TEMPO /= 10;
	Blockly.Arduino.definitions_['definition_tiratampa_'] = TTdefinition;
	Blockly.Arduino.setups_['setup_tiratampa_'] = TTsetup;
	return TTcode('velocidade','velocidade',TEMPO);
};

Blockly.Arduino.TiraTampa_Velocidade = function() {
	var VELOCIDADE = Blockly.Arduino.valueToCode(this, 'VELOCIDADE', Blockly.Arduino.ORDER_ATOMIC) || '0';
	Blockly.Arduino.definitions_['definition_tiratampa_'] = TTdefinition;
	Blockly.Arduino.setups_['setup_tiratampa_'] = TTsetup;
	var code = 'velocidade = '+VELOCIDADE+';\n';
	return code;
};

Blockly.Arduino.TiraTampa_Esquerda = function() {
	var TEMPO = Blockly.Arduino.valueToCode(this, 'TEMPO', Blockly.Arduino.ORDER_ATOMIC) || '0';
	TEMPO /= 10;
	Blockly.Arduino.definitions_['definition_tiratampa_'] = TTdefinition;
	Blockly.Arduino.setups_['setup_tiratampa_'] = TTsetup;
	return TTcode('velocidade','0',TEMPO);
};

Blockly.Arduino.TiraTampa_Direita = function() {
	var TEMPO = Blockly.Arduino.valueToCode(this, 'TEMPO', Blockly.Arduino.ORDER_ATOMIC) || '0';
	TEMPO /= 10;
	Blockly.Arduino.definitions_['definition_tiratampa_'] = TTdefinition;
	Blockly.Arduino.setups_['setup_tiratampa_'] = TTsetup;
	return TTcode('0','velocidade',TEMPO);
};

Blockly.Arduino.TiraTampa_Para = function() {
	var TEMPO = Blockly.Arduino.valueToCode(this, 'TEMPO', Blockly.Arduino.ORDER_ATOMIC) || '0';
	TEMPO /= 10;
	Blockly.Arduino.definitions_['definition_tiratampa_'] = TTdefinition;
	Blockly.Arduino.setups_['setup_tiratampa_'] = TTsetup;
	return TTcode('0','0',TEMPO);
};

Blockly.Arduino.Fim = function() {
	Blockly.Arduino.definitions_['definition_tiratampa_'] = TTdefinition;
	Blockly.Arduino.setups_['setup_tiratampa_'] = TTsetup;
	var code = 'while(true){\n'+
	'  doWifi();\n'+
	'}\n';
	return code;
};

